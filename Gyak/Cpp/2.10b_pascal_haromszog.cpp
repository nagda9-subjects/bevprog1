#include <iostream>

using namespace std;

// 2.10b) Add meg a Pascal-h�romsz�g elso N sor�t.

unsigned int factorial (int x)
{
    unsigned int fact = 1;

    for(int i = 1; i <=x; ++i)
    {
        fact *= i;
    }

    return fact;
}

int main()
{
    unsigned int n;
    cout << "Add meg hanyadik sort k�red: " << endl;
    cin >> n;

    for (int i = 0; i < n; ++i)
    {
        for (int k = 0; k <= i; ++k)
        {
            cout << factorial(i)/(factorial(i-k)*factorial(k)) << " ";
        }
        cout << endl;
    }

    return 0;
}
