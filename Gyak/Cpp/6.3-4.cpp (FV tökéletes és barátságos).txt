#include <iostream>

using namespace std;

// 6.3) Val�s�tsd meg a bool tokeletes(int a) f�ggv�nyt, ami visszaadja, hogy a param�ter�l kapott �rt�k t�k�letes sz�m-e.

// 6.4) Val�s�tsd meg a bool baratsagos(int a, int b) f�ggv�nyt, ami visszaadja, hogy a param�ter�l kapott �rt�kek bar�ts�gos sz�mp�rt alkotnak-e.

bool tokeletes(int a)
{
    int sum = 0;
    for (int i=1; i<a; i++)
    {
        if (a % i == 0)
        {
            sum += i;
        }
    }
    return (sum == a);
}

int osztok (int a)
{
    int sum = 0;
    for (int i=1; i < a; i++)
    {
        if (a % i == 0)
        {
            sum += i;
        }
    }
    return (sum);
}

bool baratsagos (int a, int b)
{
    return (a == osztok(b) && b == osztok(a));
}

int main()
{
    int a, b;
    cin >> a >> b;
    cout << boolalpha;

    cout << tokeletes (a) << endl;
    cout << tokeletes (b) << endl;
    cout << baratsagos (a,b);

    cout << noboolalpha;
    return 0;
}