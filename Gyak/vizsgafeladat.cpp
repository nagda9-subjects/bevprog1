#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

// Adott egy f�jl, amiben hallgat�k jelentkez�sei vannak g�ptermi ZH-ra. A f�jl form�tuma CSV (Comma-separated values, vesszovel elv�lasztott �rt�kek). Tartalmaz egy fejl�cet, amiben az egyes mezok nevei vannak, majd minden sora ilyen:
// 12/17/2011 1:52:21,Tiszai Bal�zs,NV6H3P,KEDD
// teh�t d�tum, vesszo, n�v, vesszo, Neptun k�d, vesszo, v�lasztott nap. Feladatok:


// a) Adj egy naponk�nti jelent�st a k�vetkezo adatokkal: h�nyan jelentkeztek keddre, h�nyan cs�t�rt�kre. Add meg a n�vsorokat (a f�jlban tal�lhat� sorrendben) a keddi alkalomra.

// b) N�h�nyan rosszul haszn�lt�k a k�rdo�vet, �s t�bbsz�r�s adatok sz�lettek. Keresd meg azokat a sorokat a f�jlban, amik olyan hallgat�r�l sz�lnak, aki m�r jelentkezett! Vigy�zat! T�bb azonos nevu hallgat� is van, �s a Neptun k�dot lehet, hogy valaki kis betuvel is �s nagy betuvel is megadta a t�bb jelentkez�s�ben!

// c) A neptunkodok.txt f�jlban vannak a neptun k�dhoz tartoz� csoportsz�mok. (megj:nem val�di csoportbeoszt�s). Felteheto, hogy legfeljebb 200 fos az �vfolyam. A program n�zze meg, hogy lehets�ges-e cs�t�rt�k�n �gy elosztani a hallgat�kat 5 darab 25 fos terembe, hogy egy csoport minden jelentkezoje egy teremben lehessen akkor, ha az algoritmus az, hogy a legkisebb csoportokat addig tessz�k egy terembe, am�g elf�rnek? (p�lda: legyen a csoportok cs�t�rt�ki l�tsz�ma emelkedo sorrendben 5 7 10 11 11 12 12 12, ilyenkor 5+7+10=22 az elso terembe, 11+11=22 a m�sodik terembe, 12+12=24 a harmadik terembe, 12 a negyedik terembe, teh�t elf�rnek.)


struct adatok{
    int nap;
    string nev;
    string neptun;
    string valnap;
    int csoport;
};

// 12/17/2011 1:52:21,Tiszai Bal�zs,NV6H3P,KEDD
adatok beker (ifstream& bf)
{
    adatok a;

    string kuka;
    bf >> ws;
    getline(bf, kuka, '/');
    bf >> a.nap;
    bf >> ws;
    getline (bf, kuka, ',');
    bf >> ws;
    getline(bf, a.nev, ',');
    bf >> ws;
    getline(bf, a.neptun, ',');
    bf >> ws;
    getline(bf, a.valnap);
    bf >> ws;   // EZ KIBASZOTT FONTOS!!!!!!!!!!

    return a;

}

void beolvas (ifstream& bf, vector<adatok>& A)
{
    string kuka;
    getline (bf, kuka);

    while (bf.good())
    {
        A.push_back(beker(bf));
    }
    bf.close();
}

void a_feladat (vector<adatok> A)
{
    int kedd_letszam = 0;
    int csut_letszam = 0;
    for (unsigned i = 0; i < A.size(); i++)
    {
        if (A[i].valnap == "KEDD")
        {
            kedd_letszam += 1;
        }
        if (A[i].valnap == "CSUTORTOK")
        {
            csut_letszam += 1;
        }
    }
    cout << "A keddi letsz�m: " << kedd_letszam << endl;
    cout << "A csutortoki letszam: " << csut_letszam << endl << endl;
    cout << "Keddre jelentkeztek:" << endl;
    for (unsigned i = 0; i < A.size(); i++)
    {
        if (A[i].valnap == "KEDD")
        {
            cout << A[i].nev << endl;
        }
    }
    cout << endl;
}

void b_feladat (vector<adatok> A)
{
    cout << "Tobbszor jelentkeztek: " << endl;
    for (unsigned i = 0; i < A.size(); i++)
    {
        for (unsigned j = i+1; j < A.size(); j++)
        {
            for (unsigned int k = 0; k < A[j].neptun.size() && k < A[i].neptun.size(); k++)
            {
                A[j].neptun[k] = toupper(A[j].neptun[k]);
                A[i].neptun[k] = toupper(A[i].neptun[k]);
            }
            if (A[i].neptun == A[j].neptun)
            {
                cout << A[i].nev << endl;
                break;
            }
        }
    }
    cout << endl;
}

adatok beker2 (ifstream& bf2)
{
    adatok b;

    bf2 >> b.neptun;
    bf2 >> ws;
    bf2 >> b.csoport;
    bf2 >> ws;

    return b;
}

void beolvas2(ifstream& bf2, vector<adatok>& B)
{
    while (bf2.good())
    {
        B.push_back(beker2(bf2));
    }
    bf2.close();
}


vector<adatok> osszefesul (vector<adatok> A, vector<adatok> B)
{
    for (unsigned i = 0; i < A.size(); i++)
    {
        for (unsigned j = 0; j < B.size(); j++)
        {
            for (unsigned k = 0; k < A[i].neptun.size(); k++)
            {
                A[i].neptun[k] = toupper(A[i].neptun[k]);
            }
            if (A[i].neptun == B[j].neptun)
            {
                A[i].csoport = B[j].csoport;
                break;
            }
        }
    }
    return A;
}

vector <int> rendez (vector<int> cs_letszam)
{
    for (int i = 1; i < cs_letszam.size(); i++)
    {
        if (cs_letszam[i-1] > cs_letszam[i])
        {
            int seged = cs_letszam[i-1];
            cs_letszam[i-1] = cs_letszam[i];
            cs_letszam[i] = seged;
            i = 0;
        }
    }

    for (int i = 0; i < cs_letszam.size(); i++)
    {
        cout << cs_letszam[i] << endl;
    }
    cout << endl;
    return cs_letszam;
}

void megoldas (vector<int> rendezett)
{
    int limit = 0;
    int csop_szam = 0;
    for (unsigned i = 0; i < rendezett.size(); i++)
    {
        limit += rendezett[i];
        if (limit > 25)
        {
            limit = rendezett[i];
            csop_szam += 1;
        }
    }
    if (limit != 0)
    {
        csop_szam += 1;
    }
    cout << csop_szam;
}

c_feladat (vector<adatok> C)
{
    vector<int> cs_letszam;
    for (int j=1; j <= 7; j++)
    {
        int sum = 0;
        for (unsigned i = 0; i < C.size(); i++)
        {
            if (C[i].valnap == "CSUTORTOK" && C[i].csoport == j )
            {
                sum += 1;
            }
        }
        cout << sum << endl;
        cs_letszam.push_back(sum);
    }
    cout << endl;
    vector <int> rendezett = rendez (cs_letszam);
    megoldas (rendezett);
}

int main()
{
    ifstream bf;
    bf.open ("jelentkezes.csv");
    if (!bf.good())
    {
        cerr << "Nem lehet megnyitni a f�jlt!";
        return 0;
    }

    vector<adatok> A;
    beolvas (bf, A);

    a_feladat (A);
    b_feladat (A);

    ifstream bf2;
    bf2.open("neptunkodok.txt");
    if (!bf2.good())
    {
        cerr << "Nem lehet megnyitni a fajlt!";
        return 0;
    }

    vector<adatok> B;
    beolvas2 (bf2, B);

    vector<adatok> C = osszefesul (A,B);

    c_feladat (C);

    return 0;
}



