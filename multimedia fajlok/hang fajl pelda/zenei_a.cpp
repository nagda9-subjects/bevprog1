#include <iostream>
#include <fstream>
#include <cmath>
using namespace std;

int main()
{
    ofstream f("a.hang");
    f << 44100 << " ";
    int hossz = 44100 * 3; // 3 masodperc hosszu
    for (int i=0;i<hossz;i++) {
        f << int(32000*                 // -32768 es 32767 kozotti ertekeket hasznalhatunk
                sin(i/44100.0*440.0*3.14*2)) << " "; // 440 Hz hang: 1 masodperc alatt 440 darab teljes periodus
    }
    f.close();
    return 0;
}
