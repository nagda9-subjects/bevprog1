#include <iostream>
#include <fstream>
#include <vector>
#include <iterator>
using namespace std;

struct Hang {
    unsigned int sf; //sampling frequency, mintavételezési frekvencia
    vector<short> d; // data, az adat
};

void saveWav(string filename, const Hang & h) {

    // mono 16 bit wav header
    unsigned char header[44] = { 0x52, 0x49, 0x46, 0x46, 0xB6, 0x3c, 0x00, 0x00, 0x57, 0x41, 0x56, 0x45, 0x66, 0x6d, 0x74, 0x20, 0x10, 0x00, 0x00, 0x00, 0x01, 0x00, 0x01, 0x00, 0x40, 0x1f, 0x00, 0x00, 0x80, 0x3e, 0x00, 0x00, 0x02, 0x00, 0x10, 0x00, 0x64, 0x61, 0x74, 0x61, 0x00};
    //setting freq
    unsigned int *p = (unsigned int *)(&header[24]);
    *p=h.sf;
    //setting length
    p = (unsigned int *)(&header[40]);
    *p=h.d.size()*2;

    //setting chucksize
    p = (unsigned int *)(&header[4]);
    *p=h.d.size()*2+36;


    string ofilename = filename + ".wav";
    ofstream fo(ofilename.c_str(), ios::binary);
    fo.write((const char *)header,44);              //header
    fo.write((const char *)&h.d[0],h.d.size()*2);   //data
    fo.close();
}


int main(int argc, char *argv[]) {
    if (argc != 2) {
        cerr << "Hasznalat: " << argv[0] << " [input.hang]" <<endl;
        return 1;
    }
    string filename = argv[1];
    cout << "reading `" << filename << "`" << endl;
    ifstream fi(filename.c_str());

    if (!fi.good()) {
        cerr << "nem letezik: `" << filename <<"`" << endl;
        return 2;
    }

    Hang h;
    fi >> h.sf;
    if (!fi.good()) {
        cerr << "ures file!" << endl;
        return 3;
    }
    copy(istream_iterator<short>(fi),istream_iterator<short>(),back_inserter<vector<short> >(h.d));
    cout << h.d.size() << " samples read" << endl;
    saveWav(filename, h);

}
