#include <iostream>
#include <fstream>
#include <vector>
#include <iterator>
#include <cstdlib>
using namespace std;

struct Hang {
    unsigned int sf; //sampling frequency, mintavételezési frekvencia
    vector<short> d; // data, az adat
};

bool loadWav(string filename, Hang & h) {
    ifstream fi(filename.c_str(), ios::binary);
    if (!fi.good()) {
        cerr << "nem letezik: `" << filename <<"`" << endl;
        return 2;
    }
    unsigned char header[44];
    fi.read((char *)header,44);

    for (int i=0;i<44;i++) printf("%x ",header[i]);

    cout << endl;
    unsigned int sf = *((unsigned int*)(&header[24]));
    unsigned short bits = *((unsigned short*)(&header[34]));
    unsigned short channels = *((unsigned short*)(&header[22]));
    unsigned short mode = *((unsigned short*)(&header[20]));
    unsigned int length = *((unsigned int*)(&header[40]))/2;
    cout << sf << "Hz " << endl;
    cout << channels << " channels " << endl;
    cout << bits << "bit " << endl;
    cout << "mode: " << mode << endl;
    cout << length << " samples " << endl;
    if (mode != 1 | channels != 1 | bits != 16) {
        cerr << "Csak 16 bites mono PCM (mode 1) wav formatum tamogatott" << endl;
        exit(4);
    }
    h.sf=sf;
    h.d.resize(length);
    fi.read((char *)(&h.d[0]),length*2);
    if (!fi.good()) {
        cerr << "Hiba olvasaskor" << endl;
        exit(6);
    }
    return fi.good();
}

int main(int argc, char *argv[]) {
    if (argc != 2) {
        cerr << "Hasznalat: " << argv[0] << " [input.wav]" <<endl;
        return 1;
    }
    string filename = argv[1];
    Hang h;
    if (loadWav(filename, h)) {
        string ofilename = filename+".hang";
        cout << ofilename;
        ofstream fo(ofilename.c_str());
        fo << h.sf << endl;
        copy(h.d.begin(), h.d.end(), ostream_iterator<short>(fo, " "));
        fo.close();
    }
}
